build:
	: please run make install
install:
	mkdir -p $(DESTDIR)/etc/init.d
	mkdir -p $(DESTDIR)/etc/conf.d
	install zram.initd $(DESTDIR)/etc/init.d/zram
	install zram.confd $(DESTDIR)/etc/conf.d/zram
