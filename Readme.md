# zram-openrc
Simple zram swap enable service for openrc

## How to install
* run `make install`
* run `rc-update add zram boot`

## Usage
* edit /etc/conf.d/zram file
* run `rc-service zram start`
